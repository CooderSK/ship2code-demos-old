package com.ship2code.demos.calculatorconditions;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input number1: ");
        float number1 = Float.parseFloat(scanner.nextLine());

        System.out.print("Input operator: ");
        String operator = scanner.nextLine();

        System.out.print("Input number2: ");
        float number2 = Float.parseFloat(scanner.nextLine());

        if (operator.equals("+")) {
            System.out.println(number1 + number2);
        } else if (operator.equals("-")) {
            System.out.println(number1 - number2);
        } else if (operator.equals("*")) {
            System.out.println(number1 * number2);
        } else if (operator.equals("/")) {
            System.out.println(number1 / number2);
        } else {
            System.out.println("Bad operator!");
        }
    }
}