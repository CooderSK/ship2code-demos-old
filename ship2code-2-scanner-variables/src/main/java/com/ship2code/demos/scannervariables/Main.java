package com.ship2code.demos.scannervariables;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.print("Input Text: ");

        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();

        System.out.println(text);
    }
}
