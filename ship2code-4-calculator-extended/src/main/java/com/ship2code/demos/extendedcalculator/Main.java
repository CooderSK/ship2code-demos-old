package com.ship2code.demos.extendedcalculator;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Welcome in Ship2Code calculator!");
        System.out.println("Input expression to get result or type \"exit\" to end.");

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print("> ");

            String expression = scanner.nextLine();

            if (expression.equals("exit") || expression.equals("stop")) {
                break;
            }

            String[] splitted = expression.split(" ");

            if (splitted.length != 3) {
                System.out.println("Bad command, please enter \"4 + 2.54\" for example.");
                continue;
            }

            if (splitted[1].length() != 1) {
                System.out.println("Bad operator, please use +, -, * or /");
            }

            float number1, number2;
            char operator;

            try {
                number1 = Float.parseFloat(splitted[0]);
                operator = splitted[1].charAt(0);
                number2 = Float.parseFloat(splitted[2]);
            } catch (Exception e) {
                System.out.println("Error in expression!");
                continue;
            }

            if (operator == '+') {
                System.out.println(number1 + number2);
            } else if (operator == '-') {
                System.out.println(number1 - number2);
            } else if (operator == '*') {
                System.out.println(number1 * number2);
            } else if (operator == '/') {
                System.out.println(number1 / number2);
            } else {
                System.out.println("Bad operator, please use +, -, * or /");
            }
        }

        System.out.println("Goodbye!");
    }
}
